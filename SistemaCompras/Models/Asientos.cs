﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaCompras.Models
{
    public class Asientos
    {
        public int EntryId;
        public String Description;
        public int AuxiliarId;
        public int AccountId;
        public String MovementType;
        public String EntryDate;
        public double EntryAmount;
        public String CurreyncyType;
    }
}