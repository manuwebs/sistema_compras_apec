﻿$(document).ready(function () {
    var actual = 0;
    document.getElementById('Total_Orden').value = "";
    $('.navbar-toggle').click(function () {
        $('.navbar-nav').toggleClass('slide-in');
        $('.side-body').toggleClass('body-slide-in');
        $('#search').removeClass('in').addClass('collapse').slideUp(200);
        /// uncomment code for absolute positioning tweek see top comment in css
        //$('.absolute-wrapper').toggleClass('slide-in');

    }); $('#Fecha_Orden').datepicker();
    var t = $('#OrdenDetalles').DataTable();

    var mensaje = 0;
    $('#OrdenDetalles').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            t.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            mensaje = 1;
        }
    });

    function showFoo2() {
        if (mensaje == 1) {
            t.row('.selected').remove().draw(false);
            mensaje = 0;
        } else {
            alert('Debe seleccionar una fila para proceder a eliminarla')
        }

    };
    var el = document.getElementById('addRow');
    el.onclick = showFoo;
    var el2 = document.getElementById('deleteRow');
    el2.onclick = showFoo2;
    var el3 = document.getElementById('create');
    el3.onclick = showFoo3;
    function showFoo3() {
        var table = $('#OrdenDetalles').DataTable();
        for (var i = 0; i < table.rows().count(); i++) {
    
            var articulo = table.row(i).data()[3];
            var cantidad = table.row(i).data()[0];
            var costo = table.row(i).data()[2];
            var ordendetalle = [cantidad, costo, articulo]
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: 'AddDetalle',
                data: { "json": ordendetalle }

            });
        }
        
     }
    function showFoo() {
        if (($('#cantidadDetalle').val() == "") || ($('#Articulo').find(":selected").text() == "") || ($('#costoDetalle').val() == "")) {
            alert('Debe rellenar todos los campos para agregar el detalle');
        } else {
            t.row.add([
                $('#cantidadDetalle').val(),
                $('#Articulo').find(":selected").text(),
                $('#costoDetalle').val(),
                $('#Articulo').find(":selected").val()
            ]).draw();
            $('#Articulo').find(":selected").remove();
            
            actual = actual + ($('#cantidadDetalle').val() * $('#costoDetalle').val())
            document.getElementById('Total_Orden').value = actual;
            $('#cantidadDetalle').val("");
            $('#costoDetalle').val("");
        }

    };




});