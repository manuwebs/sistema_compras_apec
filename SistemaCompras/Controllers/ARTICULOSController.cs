﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SistemaCompras.Models;

namespace SistemaCompras.Views
{
    public class ARTICULOSController : Controller
    {
        private SistemaComprasEntities db = new SistemaComprasEntities();

        // GET: ARTICULOS
        public ActionResult Index()
        {
            var aRTICULOS = db.ARTICULOS.Include(a => a.UNIDADES);
            return View(aRTICULOS.ToList());
        }

        // GET: ARTICULOS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.ARTICULOS aRTICULOS = db.ARTICULOS.Find(id);
            if (aRTICULOS == null)
            {
                return HttpNotFound();
            }
            return View(aRTICULOS);
        }

        // GET: ARTICULOS/Create
        public ActionResult Create()
        {
            ViewBag.Id_Unidad = new SelectList(db.UNIDADES, "Id_Unidad", "Descripcion_Unidad");
            ViewBag.Estado_Articulo = new SelectList(db.Estados, "Id_Estado", "Estado");
            return View();
        }

        // POST: ARTICULOS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Articulo,Descripcion_Articulo,Marca_Articulo,Id_Unidad,Existencia_Articulo,Estado_Articulo")] Models.ARTICULOS aRTICULOS)
        {

            var item = db.ARTICULOS.Any(x => x.Descripcion_Articulo == aRTICULOS.Descripcion_Articulo);

            if (item == false)
            {
            

                if (ModelState.IsValid)
                {
                    db.ARTICULOS.Add(aRTICULOS);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                ModelState.AddModelError("Descripcion_Articulo", "Ya existe.");
            }

            ViewBag.Id_Unidad = new SelectList(db.UNIDADES, "Id_Unidad", "Descripcion_Unidad", aRTICULOS.Id_Unidad);
            ViewBag.Estado_Articulo = new SelectList(db.Estados, "Id_Estado", "Estado", aRTICULOS.Estado_Articulo);
            return View(aRTICULOS);
        }

        // GET: ARTICULOS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.ARTICULOS aRTICULOS = db.ARTICULOS.Find(id);
            if (aRTICULOS == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_Unidad = new SelectList(db.UNIDADES, "Id_Unidad", "Descripcion_Unidad", aRTICULOS.Id_Unidad);

            ViewBag.Estado_Articulo = new SelectList(db.Estados, "Id_Estado", "Estado", aRTICULOS.Estado_Articulo);
            return View(aRTICULOS);
        }

        // POST: ARTICULOS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Articulo,Descripcion_Articulo,Marca_Articulo,Id_Unidad,Existencia_Articulo,Estado_Articulo")] Models.ARTICULOS aRTICULOS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aRTICULOS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_Unidad = new SelectList(db.UNIDADES, "Id_Unidad", "Descripcion_Unidad", aRTICULOS.Id_Unidad);
            return View(aRTICULOS);
        }

        // GET: ARTICULOS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.ARTICULOS aRTICULOS = db.ARTICULOS.Find(id);
            if (aRTICULOS == null)
            {
                return HttpNotFound();
            }
            return View(aRTICULOS);
        }

        // POST: ARTICULOS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Models.ARTICULOS aRTICULOS = db.ARTICULOS.Find(id);
            db.ARTICULOS.Remove(aRTICULOS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
