﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SistemaCompras.Models;

namespace SistemaCompras.Views
{
    public class OrdenDetallesController : Controller
    {
        private SistemaComprasEntities db = new SistemaComprasEntities();

        // GET: OrdenDetalles
        public ActionResult Index()
        {
            var ordenDetalle = db.OrdenDetalle.Include(o => o.ARTICULOS).Include(o => o.OrdenCabecera);
            return View(ordenDetalle.ToList());
        }

        // GET: OrdenDetalles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrdenDetalle ordenDetalle = db.OrdenDetalle.Find(id);
            if (ordenDetalle == null)
            {
                return HttpNotFound();
            }
            return View(ordenDetalle);
        }

        // GET: OrdenDetalles/Create
        public ActionResult Create()
        {
            ViewBag.Id_Articulo = new SelectList(db.ARTICULOS, "Id_Articulo", "Descripcion_Articulo");
            ViewBag.Id_OrdenCabecera = new SelectList(db.OrdenCabecera, "Id_OrdenCabecera", "Id_OrdenCabecera");
            return View();
        }

        // POST: OrdenDetalles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_OrdenCabecera,Id_Articulo,Cantidad_Orden,Costo_Orden")] OrdenDetalle ordenDetalle)
        {
            if (ModelState.IsValid)
            {
                db.OrdenDetalle.Add(ordenDetalle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Id_Articulo = new SelectList(db.ARTICULOS, "Id_Articulo", "Descripcion_Articulo", ordenDetalle.Id_Articulo);
            ViewBag.Id_OrdenCabecera = new SelectList(db.OrdenCabecera, "Id_OrdenCabecera", "Id_OrdenCabecera", ordenDetalle.Id_OrdenCabecera);
            return View(ordenDetalle);
        }

        // GET: OrdenDetalles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrdenDetalle ordenDetalle = db.OrdenDetalle.Find(id);
            if (ordenDetalle == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_Articulo = new SelectList(db.ARTICULOS, "Id_Articulo", "Descripcion_Articulo", ordenDetalle.Id_Articulo);
            ViewBag.Id_OrdenCabecera = new SelectList(db.OrdenCabecera, "Id_OrdenCabecera", "Id_OrdenCabecera", ordenDetalle.Id_OrdenCabecera);
            return View(ordenDetalle);
        }

        // POST: OrdenDetalles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_OrdenCabecera,Id_Articulo,Cantidad_Orden,Costo_Orden")] OrdenDetalle ordenDetalle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ordenDetalle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Id_Articulo = new SelectList(db.ARTICULOS, "Id_Articulo", "Descripcion_Articulo", ordenDetalle.Id_Articulo);
            ViewBag.Id_OrdenCabecera = new SelectList(db.OrdenCabecera, "Id_OrdenCabecera", "Id_OrdenCabecera", ordenDetalle.Id_OrdenCabecera);
            return View(ordenDetalle);
        }

        // GET: OrdenDetalles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrdenDetalle ordenDetalle = db.OrdenDetalle.Find(id);
            if (ordenDetalle == null)
            {
                return HttpNotFound();
            }
            return View(ordenDetalle);
        }

        // POST: OrdenDetalles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrdenDetalle ordenDetalle = db.OrdenDetalle.Find(id);
            db.OrdenDetalle.Remove(ordenDetalle);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
