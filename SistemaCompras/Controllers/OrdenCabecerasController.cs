﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SistemaCompras.Models;
using Newtonsoft.Json;
using System.IO;
using System.Web.Script.Serialization;
using System.Net.Http;

namespace SistemaCompras.Views
{
    public class OrdenCabecerasController : Controller
    {
        private SistemaComprasEntities db = new SistemaComprasEntities();
        static List<OrdenDetalle> DetalleList;
        // GET: OrdenCabeceras
        public ActionResult EnviarAsiento()
        {
            var oc = db.OrdenCabecera.Include(o => o.DEPARTAMENTOS).Include(o => o.PROVEEDORES).Include(o => o.Estados).Where(x => x.Asentada == null || x.Asentada == 0).ToList();
            
            Double total=0;
            foreach (var c in oc)
            {
                total += double.Parse(c.Total_Orden.ToString());
            }
           
            Asientos asiento = new Asientos();
            asiento.Description = "Asiento enviado desde compras";
            asiento.AuxiliarId = 7;
            asiento.AccountId = 1;
            asiento.MovementType = "CR";
            asiento.EntryDate = "2017-12-07T20:27:48.0564331+00:00";
            asiento.EntryAmount = total;
            asiento.CurreyncyType = "DOP";
            string json = new JavaScriptSerializer().Serialize(asiento);
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://accountingsystem.azurewebsites.net/API/Entries");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            //using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
            //{
            //    client.BaseAddress = new Uri("http://url goes here/");
            //    var body = new Dictionary<string, string>();
            //    body.Add("json", json);
            //    var post = new FormUrlEncodedContent(body);

            //    client.PostAsync(client.BaseAddress + "/route /goes here ", post).ContinueWith(
            //        task =>
            //    {
            //        var responeNew = task.Result;
     
            //        //LOGICA AQUI.
            //    });

                
            //}

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                var result2 = new JavaScriptSerializer().Deserialize<Asientos>(result);
                int identry = result2.EntryId;
                foreach (var c in oc)
                {
                   c.Asentada = identry;
                    db.Entry(c).State = EntityState.Modified;
                }
            }
            db.SaveChanges();
            return RedirectToAction("Asentar");

        }
        public ActionResult Index()
        {
            
            var ordenCabecera = db.OrdenCabecera.Include(o => o.DEPARTAMENTOS).Include(o => o.PROVEEDORES).Include(o => o.Estados);
            return View(ordenCabecera.ToList());
        }
        // GET: OrdenCabeceras not asented
        public ActionResult Asentar()
        {
            var ordenCabecera = db.OrdenCabecera.Include(o => o.DEPARTAMENTOS).Include(o => o.PROVEEDORES).Include(o => o.Estados).Where(x => x.Asentada == null); 
            return View(ordenCabecera.ToList());
        }
        
        // GET: OrdenCabeceras/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrdenCabecera ordenCabecera = db.OrdenCabecera.Find(id);
            if (ordenCabecera == null)
            {
                return HttpNotFound();
            }
            return View(ordenCabecera);
        }
        [HttpPost]
        public ActionResult AddDetalle(String[] Json)
        {
            
            OrdenDetalle detalleitem = new OrdenDetalle();
            detalleitem.Cantidad_Orden = int.Parse(Json[0]);
            detalleitem.Costo_Orden = int.Parse(Json[1]);
            detalleitem.Id_Articulo = int.Parse(Json[2]);
            if(DetalleList == null)
            {
                DetalleList = new List<OrdenDetalle>();
            }
            DetalleList.Add(detalleitem);
            return null;
        }
        
        // GET: OrdenCabeceras/Create
        public ActionResult Create()
        {
                    
        ViewBag.Articulo = new SelectList(db.ARTICULOS, "Id_Articulo", "Descripcion_Articulo");
            ViewBag.Id_Departamento = new SelectList(db.DEPARTAMENTOS, "Id_Departamento", "Nombre_Departamento");
            ViewBag.Id_Proveedor = new SelectList(db.PROVEEDORES, "Id_Proveedor", "NombreComercial");
            ViewBag.Estado_Orden = new SelectList(db.Estados, "Id_Estado", "Estado");
            OrdenCabecera oc = new OrdenCabecera();

            return View();
        }

        // POST: OrdenCabeceras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_OrdenCabecera,Numero_Orden,Fecha_Orden,Id_Departamento,Id_Proveedor,Estado_Orden,Total_Orden")] OrdenCabecera ordenCabecera)
        {
            var custome = db.OrdenCabecera.Any(x => x.Numero_Orden == ordenCabecera.Numero_Orden);

            if (custome==false)
            {
            
                if (ModelState.IsValid)
                {
                    db.OrdenCabecera.Add(ordenCabecera);
                    foreach(var item in DetalleList)
                    {
                        item.Id_OrdenCabecera = ordenCabecera.Id_OrdenCabecera;
                        db.OrdenDetalle.Add(item);
                        DetalleList = null;
                    }
                    db.SaveChanges();
                    return RedirectToAction("/Index");
                    
                }
            }
            else
            {
                ModelState.AddModelError("Numero_Orden", "Ya existe.");
            }
            ViewBag.Articulo = new SelectList(db.ARTICULOS, "Id_Articulo", "Descripcion_Articulo", null);
            ViewBag.Id_Departamento = new SelectList(db.DEPARTAMENTOS, "Id_Departamento", "Nombre_Departamento", ordenCabecera.Id_Departamento);
            ViewBag.Id_Proveedor = new SelectList(db.PROVEEDORES, "Id_Proveedor", "NombreComercial", ordenCabecera.Id_Proveedor);
            ViewBag.Estado_Orden = new SelectList(db.Estados, "Id_Estado", "Estado", ordenCabecera.Estado_Orden);
            return View(ordenCabecera);
        }

        // GET: OrdenCabeceras/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrdenCabecera ordenCabecera = db.OrdenCabecera.Find(id);
            if (ordenCabecera == null)
            {
                return HttpNotFound();
            }
            ViewBag.Id_Departamento = new SelectList(db.DEPARTAMENTOS, "Id_Departamento", "Nombre_Departamento", ordenCabecera.Id_Departamento);
            ViewBag.Id_Proveedor = new SelectList(db.PROVEEDORES, "Id_Proveedor", "NombreComercial", ordenCabecera.Id_Proveedor);
            ViewBag.Estado_Orden = new SelectList(db.Estados, "Id_Estado", "Estado", ordenCabecera.Estado_Orden);
            return View(ordenCabecera);
        }

        // POST: OrdenCabeceras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_OrdenCabecera,Numero_Orden,Fecha_Orden,Id_Departamento,Id_Proveedor,Estado_Orden,Total_Orden")] OrdenCabecera ordenCabecera)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ordenCabecera).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("/Index");
            }
            ViewBag.Id_Departamento = new SelectList(db.DEPARTAMENTOS, "Id_Departamento", "Nombre_Departamento", ordenCabecera.Id_Departamento);
            ViewBag.Id_Proveedor = new SelectList(db.PROVEEDORES, "Id_Proveedor", "NombreComercial", ordenCabecera.Id_Proveedor);
            ViewBag.Estado_Orden = new SelectList(db.Estados, "Id_Estado", "Estado", ordenCabecera.Estado_Orden);
            return View(ordenCabecera);
        }

        // GET: OrdenCabeceras/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrdenCabecera ordenCabecera = db.OrdenCabecera.Find(id);
            if (ordenCabecera == null)
            {
                return HttpNotFound();
            }
            return View(ordenCabecera);
        }

        // POST: OrdenCabeceras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrdenCabecera ordenCabecera = db.OrdenCabecera.Find(id);
            if (ordenCabecera.OrdenDetalle != null)
            {
                ICollection<OrdenDetalle> ordenDetallec = ordenCabecera.OrdenDetalle;
                List<OrdenDetalle> ordenDetallel = new List<OrdenDetalle>(ordenDetallec);
                for (int i = 0; i < ordenDetallel.Count(); i++)
                {
                    db.OrdenDetalle.Remove(ordenDetallel[i]);
                }
            }
            db.OrdenCabecera.Remove(ordenCabecera);
            
            db.SaveChanges();
            return RedirectToAction("/Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
