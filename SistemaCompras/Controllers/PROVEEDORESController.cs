﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SistemaCompras.Models;

namespace SistemaCompras.Views
{
    public class PROVEEDORESController : Controller
    {
        private SistemaComprasEntities db = new SistemaComprasEntities();

        // GET: PROVEEDORES
        public ActionResult Index()
        {
            return View(db.PROVEEDORES.ToList());
        }

        // GET: PROVEEDORES/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PROVEEDORES pROVEEDORES = db.PROVEEDORES.Find(id);
            if (pROVEEDORES == null)
            {
                return HttpNotFound();
            }
            return View(pROVEEDORES);
        }

        // GET: PROVEEDORES/Create
        public ActionResult Create()
        {
            ViewBag.Estado = new SelectList(db.Estados, "Id_Estado", "Estado");
            return View();
        }

        // POST: PROVEEDORES/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Proveedor,CedulaRNC,NombreComercial,Estado")] PROVEEDORES pROVEEDORES)
        {
            var item = db.PROVEEDORES.Any(x => x.CedulaRNC == pROVEEDORES.CedulaRNC);

            if (item == false)
            {
                var items = db.PROVEEDORES.Any(x => x.NombreComercial == pROVEEDORES.NombreComercial);

                if (items == false)
                {

                    if (ModelState.IsValid)
                    {
                        db.PROVEEDORES.Add(pROVEEDORES);
                        db.SaveChanges();
                        return RedirectToAction("/Index");
                    }
                }
                else
                {
                    ModelState.AddModelError("NombreComercial", "Ya existe.");
                }
            }
            else
            {
                ModelState.AddModelError("CedulaRNC", "Ya existe.");
            }
                ViewBag.Estado = new SelectList(db.Estados, "Id_Estado", "Estado",pROVEEDORES.Estado);
            return View(pROVEEDORES);
        }

        // GET: PROVEEDORES/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PROVEEDORES pROVEEDORES = db.PROVEEDORES.Find(id);
            if (pROVEEDORES == null)
            {
                return HttpNotFound();
            }
            ViewBag.Estado = new SelectList(db.Estados, "Id_Estado", "Estado", pROVEEDORES.Estado);
            return View(pROVEEDORES);
        }

        // POST: PROVEEDORES/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Proveedor,CedulaRNC,NombreComercial,Estado")] PROVEEDORES pROVEEDORES)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pROVEEDORES).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("/Index");
            }
            return View(pROVEEDORES);
        }

        // GET: PROVEEDORES/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PROVEEDORES pROVEEDORES = db.PROVEEDORES.Find(id);
            if (pROVEEDORES == null)
            {
                return HttpNotFound();
            }
            return View(pROVEEDORES);
        }

        // POST: PROVEEDORES/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PROVEEDORES pROVEEDORES = db.PROVEEDORES.Find(id);
            db.PROVEEDORES.Remove(pROVEEDORES);
            db.SaveChanges();
            return RedirectToAction("/Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
