﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SistemaCompras.Models;

namespace SistemaCompras.Views
{
    public class UNIDADESController : Controller
    {
        private SistemaComprasEntities db = new SistemaComprasEntities();
        private string Descripcion_Unidad;

        // GET: UNIDADES
        public ActionResult Index()
        {
            return View(db.UNIDADES.ToList());
        }

        // GET: UNIDADES/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UNIDADES uNIDADES = db.UNIDADES.Find(id);
            if (uNIDADES == null)
            {
                return HttpNotFound();
            }
            return View(uNIDADES);
        }

        // GET: UNIDADES/Create
        public ActionResult Create()
        {
            ViewBag.Estado_Unidad = new SelectList(db.Estados, "Id_Estado", "Estado");
            return View();
        }

        // POST: UNIDADES/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Unidad,Descripcion_Unidad,Estado_Unidad")] UNIDADES uNIDADES)
        {
            var item = db.UNIDADES.Any(x => x.Descripcion_Unidad == uNIDADES.Descripcion_Unidad);

            if (item == false)
            {
            

            if (ModelState.IsValid)
                {
                    db.UNIDADES.Add(uNIDADES);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                ModelState.AddModelError("Descripcion_Unidad", "Ya existe.");
            }
            ViewBag.Estado_Unidad = new SelectList(db.Estados, "Id_Estado", "Estado", uNIDADES.Estado_Unidad);
            return View(uNIDADES);
        }


        // GET: UNIDADES/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UNIDADES uNIDADES = db.UNIDADES.Find(id);
            if (uNIDADES == null)
            {
                return HttpNotFound();
            }
            return View(uNIDADES);
        }

        // POST: UNIDADES/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Unidad,Descripcion_Unidad,Estado_Unidad")] UNIDADES uNIDADES)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uNIDADES).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uNIDADES);
        }

        // GET: UNIDADES/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UNIDADES uNIDADES = db.UNIDADES.Find(id);
            if (uNIDADES == null)
            {
                return HttpNotFound();
            }
            return View(uNIDADES);
        }

        // POST: UNIDADES/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UNIDADES uNIDADES = db.UNIDADES.Find(id);
            db.UNIDADES.Remove(uNIDADES);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
