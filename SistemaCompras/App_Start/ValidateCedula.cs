﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SistemaCompras.Models;

namespace System.ComponentModel.DataAnnotations {
   sealed public class ValidateCedula
    {
        public static ValidationResult Validar(String PropertyName)
        {
            bool isValid;
            int vnTotal = 0;
            string vcCedula = PropertyName.Replace("-", "");
            int pLongCed = vcCedula.Trim().Length;
            int[] digitoMult = new int[11] { 1, 2, 1, 2, 1, 2, 1, 2, 1, 2, 1 };

            if (pLongCed < 11 || pLongCed > 11) {
                return new ValidationResult("El comprobante no cumple con la longitud esperada.");
            }


            for (int vDig = 1; vDig <= pLongCed; vDig++)
            {
                int vCalculo = Int32.Parse(vcCedula.Substring(vDig - 1, 1)) * digitoMult[vDig - 1];
                if (vCalculo < 10) { vnTotal += vCalculo; }

                else {
                    vnTotal += Int32.Parse(vCalculo.ToString().Substring(0, 1)) + Int32.Parse(vCalculo.ToString().Substring(1, 1));
                }
            }

            if (vnTotal % 10 == 0) { isValid = true; }


            else { isValid = false; }
            if (isValid)
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("El comprobante es invalido.");
            }


        }
    }
}